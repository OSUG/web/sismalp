<?php

return [
    'home'          => [
        'action' => [HomeController::class, 'home']
    ],
    'mentions_legales'  => [
        'action' => [HomeController::class, 'legalesMentions']
    ],
    '404'  => [
        'action' => [ErrorController::class, 'NotFoundPage']
    ],
    'register'  => [
        
        'action' => [AuthorController::class, 'add']
    ],
    'author'  => [
        
        'action' => [AuthorController::class, 'update']
    ],
    'manage_authors'  => [
        
        'action' => [AuthorController::class, 'manage']
    ],
    'delete_author'  => [
        
        'action' => [AuthorController::class, 'delete']
    ],
    'logout'  => [
        
        'action' => [AuthorController::class, 'connect']
    ],
    'login'  => [
        
        'action' => [AuthorController::class, 'connect']
    ],
    'add_page'  => [
        
        'action' => [PageController::class, 'add']
    ],
    'add_contact_type'  => [
        
        'action' => [ContactController::class, 'add']
    ],
    // 'contact'  => [
        
    //     'action' => [MessageController::class, 'add']
    // ],
    'manage_requests'  => [
        
        'action' => [MessageController::class, 'manage']
    ],
    'delete_message'  => [
        
        'action' => [MessageController::class, 'delete']
    ],
    'archive_message'  => [
        
        'action' => [MessageController::class, 'toggleArchive']
    ],
    'unarchive_message'  => [
        
        'action' => [MessageController::class, 'toggleArchive']
    ],
    'add_article'  => [
        
        'action' => [ArticleController::class, 'add']
    ],
    'manage_articles'  => [
        
        'action' => [ArticleController::class, 'manage']
    ],
    'select_article_by_id' => [
        
        'action' => [ArticleController::class, 'selectArticleById']
    ],
    'update_article'  => [
        
        'action' => [ArticleController::class, 'update']
    ],
    'delete_article'  => [
        
        'action' => [ArticleController::class, 'delete']
    ],
    'a_propos'  => [
        
        'action' => [ArticleController::class, 'show']
    ],
    'partenaires'  => [
        
        'action' => [ArticleController::class, 'show']
    ],
    'template_new_page'  => [ // Rename route name to match the one called in article controller 
        
        'action' => [ArticleController::class, 'show']
    ],
    'select_events'  => [
        'action' => [EventController::class, 'request']
    ],
];
