<?php

class Router {
    
    private array $routes;
    
    public function __construct() {
        $this->routes = require './configs/routes.php';
    }
    
    public function callActionFromRequest(): void {
        
        if(isset($_GET['page']) && !empty($_GET['page'])) {
            $route = $this->routes[$_GET['page']]  ?? $this->routes['404'];
        } else {
            $route = $this->routes['home'];
        }
        
        // $route : ['action' => [HomeController::class, 'home']]
        
        // $controller = new HomeController;
        $controller = new $route['action'][0];
        
        // $controller->home();
        $controller->{$route['action'][1]}();
    }
    
    public function isAjax() {
                
        $ajaxActions = [
            'select_article_by_id',
            'select_events'
        ];  
                
        if(isset($_GET['page']) && in_array($_GET['page'], $ajaxActions)){
            return true;
        } else {
            return false;
        }
                
    }
}
