'use strict';

/***********************************************************************************/
/* *********************************** DATA *************************************/
/***********************************************************************************/
let cssData = {
    fsDefault: "1.6rem",
    lhDefault: "2.0rem",
    backgroundColor: "#ffffff",
    color: "#333333",
    titleColor: "#333333",
    imgBackground: "none",
    imgBorderRadius: "none",
    linkColor: "none",
};

                    
/***********************************************************************************/
/* *************************** DOM ELEMENT SELECTIONS ***************************/
/***********************************************************************************/

// Select DOM Element Accessibily Menu
let selectAccessibilityMenu = document.querySelector('.accessibility-menu');
let chevron = document.querySelector('.chevron-down');
let accessibilityMenu = document.querySelector('.select-accessibility-menu');
let policeCheckbox = document.getElementById("policeCheckbox");
let lineheightCheckbox = document.getElementById("lineheightCheckbox");
let contrastCheckbox = document.getElementById("contrastCheckbox");
let gradientStops = document.querySelectorAll("linearGradient stop");
let textElements = document.querySelectorAll("g text");

// Select DOM Element Burger Menu and home title
let list = document.getElementById("mainNav");
let button = document.getElementById("burgerMenu");
let h2TitleHome = document.querySelectorAll(".header-home h2[data-target]");

// Select specific Id and class for modal elements
let deleteForms = document.querySelectorAll(".delete-form");
let confirmationModal = document.getElementById("confirmationModal");
let confirmDeleteInput = document.getElementById("confirmDelete");
let cancelDeleteInput = document.getElementById("cancelDelete");

// Select specific Id and class for map display Events and table
let map = document.querySelector("#map");
let last24Checkbox = document.getElementById("last24Checkbox");
let earthquakeCheckbox = document.getElementById("earthquakeCheckbox");
let otherEventsCheckbox = document.getElementById("otherEventsCheckbox");
let mapMarkers = []; // Markers Array
let earthquakeData = [];
let tbody = document.getElementById("LastEarthquakeEvents");
let screenWidth = window.innerWidth;

// Select specific class backoffice elements
let updateLinks = document.querySelectorAll(".action-article-btn");
let articleTitle = document.querySelector("#addArticleContainer h3");
let articleFormAction = document.querySelector("#addArticleContainer form");
let submitArticleInput = document.getElementById("addArticleButton");
let pageSelect = document.getElementById("pageId");
let titleInput = document.getElementById("title");
let articleIdInput = document.getElementById("articleId");
let contentTextarea = document.getElementById("content");
let urlLinkInput = document.getElementById("linkUrl");
let nameLinkInput = document.getElementById("linkName");
let imgDdl = document.getElementById("ddlImg");
let imageLabel = document.getElementById("imageLabel"); 

/***********************************************************************************/
/* *************************** MAIN CODE ***************************/
/***********************************************************************************/

document.addEventListener("DOMContentLoaded", function(){
    
    try {
        // Retrieve the stored accessibility preferences from the localStorage
        let storedPreferences = localStorage.getItem('accessibilityPreferences');
        
        // Check if accessibility preferences were previously stored
        if(storedPreferences) {
            // Parse the stored preferences from a JSON string into an object
            cssData = JSON.parse(storedPreferences);
            
            // Restore the state of checkboxes based on the stored preferences
            policeCheckbox.checked = cssData.fsDefault === "2.0rem";
            lineheightCheckbox.checked = cssData.lhDefault === "3.0rem";
            contrastCheckbox.checked = cssData.backgroundColor === "#333333";
            
            // Restore other accessibility preferences
            gradientStops.forEach(stop => {
                stop.setAttribute("stop-color", cssData.color);
            });
            textElements.forEach(text => {
                text.style.fill = cssData.color;
            });
            
            // Apply the restored accessibility preferences to the website's styles
            updateAccessibilityOnWebsite();    
        }
        
        /***************************************************************************************/
        //************************** Accessibility Menu ************************************* //

        // Add change event listeners to various checkboxes to update accessibility
        policeCheckbox.addEventListener('change', updateAccessibility);
        lineheightCheckbox.addEventListener('change', updateAccessibility);
        contrastCheckbox.addEventListener('change', updateAccessibility);
    
        // Add a click and keyup event listener to the accessibilityMenu element
        accessibilityMenu.addEventListener('click', toggleAccessibilityMenu);
        accessibilityMenu.addEventListener("keyup", function(e) {
            if (e.code === "Enter") {
                    e.preventDefault();
                    accessibilityMenu.focus();
                    toggleAccessibilityMenu();
            }
        });
        
        /***********************************************************************************/
        //************************** Buger Menu ***************************************** //

        // Add a click event listener to the burger menu button
        button.addEventListener("click", function(e){
            e.preventDefault();
            toggleVisibility(button, list);
        });
            
        // Add a keyup event listener to the navigation list
        list.addEventListener("keyup", function(e) {
            if (e.code === "Escape") {
                e.preventDefault();
                button.focus();
                toggleVisibility(button, list);
            }
        });

        /***********************************************************************************/
        //************************** Icon Captcha ***************************************** //
        
        // IconCaptcha.init('.iconcaptcha-widget', {
        //     general: {
        //         endpoint: './iconcaptcha/captcha-request.php', // required, change the path accordingly.
        //         fontFamily: 'inherit',
        //         credits: 'show',
        //     },
        //     security: {
        //         interactionDelay: 1500,
        //         hoverProtection: true,
        //         displayInitialMessage: true,
        //         initializationDelay: 500,
        //         incorrectSelectionResetDelay: 3000,
        //         loadingAnimationDuration: 1000,
        //     },
        //     locale: {
        //         initialization: {
        //             verify: 'Verify that you are human.',
        //             loading: 'Loading challenge...',
        //         },
        //         header: 'Select the image displayed the <u>least</u> amount of times',
        //         correct: 'Verification complete.',
        //         incorrect: {
        //             title: 'Uh oh.',
        //             subtitle: "You've selected the wrong image.",
        //         },
        //         timeout: {
        //             title: 'Please wait.',
        //             subtitle: 'You made too many incorrect selections.'
        //         }
        //     }
        // });
        
        /***********************************************************************************/
        //************************** Title home interaction ***************************** //

        // Iterate through h2TitleHome elements
        h2TitleHome.forEach((h2Title) => {
            let targetIdTitleHome = h2Title.getAttribute('data-target');
            let associatedContainer = document.getElementById(targetIdTitleHome);
            
            // Add a click event listener to each h2Title element
            h2Title.addEventListener("click", function(e) {
                e.preventDefault();
                toggleVisibility(h2Title, associatedContainer);
            });  
                
            // Add an Enter key event listener to each h2Title element
            h2Title.addEventListener("keyup", function(e) {
                if (e.code === "Enter") {
                    e.preventDefault();
                    h2Title.focus();
                    toggleVisibility(h2Title, associatedContainer);
                }
            });
        });
        
        /***********************************************************************************/
        //**************************  Modal  ******************************************** //

        deleteForms.forEach(form => {
            form.addEventListener("submit", (e) => {
                e.preventDefault();
                // Open the confirmation modal when a delete form is submitted
                openModal(form);
            });
        });
        
        /***********************************************************************************/
        //**************************  Display Events on Map ***************************** //
        
        // Check if the element with ID "map" exists in the document
        if (document.querySelector("#map") !== null) {     
    
            // Initialize coordinates for centering the map 
            let lat = 45.188529;
            let lon = 5.724524;
            
            // Create a Leaflet map with this coordinates and level zoom
            map = L.map("map").setView([lat, lon], 8);
            
            L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
            	minZoom : 3,
            	maxZoom: 12,
            	attribution: 'Esri, HERE, Garmin, Intermap, increment P Corp., GEBCO, USGS, FAO, NPS, NRCAN, GeoBase, IGN, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), (c) OpenStreetMap contributors, and the GIS User Community'
            }).addTo(map);

              
            // L.tileLayer('https://wxs.ign.fr/{apikey}/geoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE={style}&TILEMATRIXSET=PM&FORMAT={format}&LAYER=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}', {
            // 	attribution: 'Carte © IGN-F/Geoportail',
            // 	minZoom: 13,
            // 	maxZoom: 18,
            // 	apikey: 'decouverte',
            // 	format: 'image/png',
            // 	style: 'normal',
            // 	service: 'WMTS',
            // }).addTo(map);

            L.tileLayer('https://data.geopf.fr/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2&TILEMATRIXSET=PM&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&apikey={apikey}&FORMAT={format}&STYLE=normal', {
            	attribution: 'Carte © IGN-F/Geoportail',
            	minZoom: 13,
            	maxZoom: 18,
            	apikey: 'sbba056l90k9y80ougqpdl1m',
            	format: 'image/png',
            	style: 'normal',
            	service: 'WMTS',
            }).addTo(map);
    
            last24Checkbox.addEventListener('change', updateMarkersOnMap);
            earthquakeCheckbox.addEventListener('change', updateMarkersOnMap);
            otherEventsCheckbox.addEventListener('change', updateMarkersOnMap);

            // Ajax request to fetch event data from webserver
            function updateData() {
                
                mapMarkers.forEach(marker => {
                    map.removeLayer(marker);
                });
            
                // Clean markers array
                mapMarkers = [];
                
                fetch('?page=select_events')
                    .then(response => response.json())
                    .then(data => {
                        // console.log(data);
                        // Iterate through each event data
                        data.forEach(event => {
                        
                            // pick up data from each event (lat, lon, magnitude, depth, evaluationMode, type)
                            lat = parseFloat(event.latitude);
                            lon = parseFloat(event.longitude);
                            let magnitude = parseFloat(event.magnitude).toFixed(2);
                            let depth = (parseFloat(event.depth) / 1000).toFixed(1);
                            let evaluationMode = event.evaluationMode;
                            let type = event.type;
                            let evaluationMessage = ""; // Initialize and create a specific message for automatic event
        
                            
                            // pick up date from each event and calculate is age to determine opacity
                            let eventDate = new Date(event.time);
                            let currentDate = new Date();
                            let ageEvent = (currentDate - eventDate) / (1000*60*60*24);
                                                
                            let dateData = {
                                day: formattedNumber(eventDate.getDate()),
                                month: formattedNumber(eventDate.getMonth() + 1),
                                year: eventDate.getFullYear(),
                                hours: formattedNumber(eventDate.getHours()),
                                minutes: formattedNumber(eventDate.getMinutes()),
                                seconds: formattedNumber(eventDate.getSeconds()),
                            };
                                                
                            let opacity = 1 - (ageEvent / 30);
                            // let opacity = 1 * Math.exp(-ageEvent / 30);
                            // opacity = Math.max(0.2, opacity);
        
                            // Initialize variables for icon-related properties
                            let customIcon = '';
        
                            // Check if latitude, longitude, and magnitude are valid numbers
                            if (!isNaN(lat) && !isNaN(lon) && !isNaN(magnitude)) {
        
                                if(event.type === "earthquake") {
                                    
                                    type = "Séisme"; // Set the event type to "Séisme"
                                    customIcon = generateEarthquakeIcon(magnitude);
                                   
                                    // To create a table of big events earthquake
                                    if (ageEvent < 30 && magnitude > 2) {
                                        earthquakeData.push({
                                            date: `Le ${dateData.day}/${dateData.month}/${dateData.year} à ${dateData.hours}:${dateData.minutes}:${dateData.seconds}`,
                                            region: `${event.description}
                                            (coordonnées: ${lat}, ${lon})`,
                                            magnitude: magnitude,
                                            depth: `${depth} km`,
                                            ageEvent: ageEvent
                                        });
                                    }
                                    
                                } else {
                                   let iconResult = generateOtherCustomIcon(event.type);
                                   customIcon = iconResult.icon;  // Set the icon URL and event type for all other events
                                   type = iconResult.type;
                                } 
                            
                                if (evaluationMode === "automatic") {
                                    evaluationMessage = "Cet événement n'a pas été révisé par un scientifique";
                                }
                                
                                if (ageEvent < 1) {
                                    customIcon.options.className = 'flash-icon'; // Create a specific class for event from the day
                                }
                                
                                let marker = L.marker([lat, lon], {icon: customIcon, opacity: opacity}).addTo(map);  // Create a marker on the map with the custom icon and opacity
                                
                                // Create a table of map markers for updateMarkersOnMap function
                                marker.type = event.type; 
                                marker.customOpacity = opacity; 
                                marker.ageEvent = ageEvent;
                                mapMarkers.push(marker); // Add marker to array
            
                                // Bind a popup to the marker with event details including the evaluation message
                                marker.bindPopup(`<b>${event.description}</b>
                                    <br>Le <b>${dateData.day}/${dateData.month}/${dateData.year}</b> à ${dateData.hours}:${dateData.minutes}:${dateData.seconds}<br>
                                    Magnitude: ${magnitude}<br>
                                    Profondeur: ${depth} km<br>
                                    Type: ${type}<br><br>
                                    <b>${evaluationMessage}</b>`, {
                                    popupAnchor: [0, -20] 
                                });
                            }
                        });
                        // Hide otherEvents at beggining
                        otherEventsCheckbox.checked = true;
                        otherEventsCheckbox.dispatchEvent(new Event('change'));

                        earthquakeCheckbox.checked = true; 
                        earthquakeCheckbox.dispatchEvent(new Event('change')); 
                        
                        //Add earthquake event on html array
                        earthquakeData.sort((a,b) => a.ageEvent - b.ageEvent);
                                    
                        let recent5Events = earthquakeData.slice(0, 5);
                        
                        recent5Events.forEach(function(event) {
                            let row = tbody.insertRow();
            
                            let dateCell = row.insertCell(0);
                            dateCell.textContent = event.date;
                                    
                            let regionCell = row.insertCell(1);
                            regionCell.textContent = event.region;
                                    
                            let magnitudeCell = row.insertCell(2);
                            if (screenWidth < 1280) {
                                magnitudeCell.textContent = `Magnitude: ${event.magnitude}`;
                            } else {
                                magnitudeCell.textContent = event.magnitude;
                            }
                                    
                            let depthCell = row.insertCell(3);
                            if (screenWidth < 1280) {
                                depthCell.textContent = `${event.depth} de profondeur`;
                            } else {
                                depthCell.textContent = event.depth;
                            }
                        });
                    })
                    .catch(error => {
                            console.error(error);
                    });
            }
            
            // Call up the initial data update function
            updateData();
            
            // Update data every 15 minutes (15 * 60 * 1000 milliseconds)
            setInterval(updateData, 15 * 60 * 1000);
            
        }
        
        /***************************************************************************************/
        //**************************  BACKOFFICE Manage/Update Article ********************** //
        
        // Iterate through each selected element
        updateLinks.forEach(link => {
            link.addEventListener("click", (e) => {                         // Add a click event listener to each element
                e.preventDefault();                                         // Prevent the default click behavior
                let articleId = link.getAttribute("data-article-id");       // Get the articleId from the "data-article-id" attribute
                
                // Ajax request to fetch article data 
                let myRequest = new Request("?page=select_article_by_id", {
                    method : 'POST',
                    body : JSON.stringify({ articleIdResult : articleId })
                });
                
                fetch(myRequest)
                .then(response => response.json())
                .then(result => {
                    
                    // Extract data from the response
                    let articleData = result.articleData;
                    let allPages = result.allPages;
                    
                    // Update the article title
                    articleTitle.textContent = "Mettre à jour l'article";
                    
                    // Update the form action attribute
                    articleFormAction.setAttribute("action", "index.php?page=update_article");
                    
                    // Update the submit button label
                    submitArticleInput.value = "Mettre à jour";
                    
                    // Update elements on the page with the retrieved data
                    
                    // Update the page selection dropdown
                    pageSelect.selectedIndex = 0; 
                    pageSelect.innerHTML = ''; 
                    allPages.forEach(page => {
                        let option = document.createElement('option');
                        option.value = page.id;  // Set the page ID as the option value
                        option.textContent = page.name;
                        pageSelect.appendChild(option);
                    });
                    
                    let selectedPage = articleData.page;
    
                    for (let i = 0; i < pageSelect.options.length; i++) {
                      if (pageSelect.options[i].textContent === selectedPage) {
                        pageSelect.selectedIndex = i;
                        break; 
                      }
                    }
       
                    titleInput.value = articleData.title;           // Update the title input field   
                    articleIdInput.value = articleData.id;          // Update the article ID input field           
                    contentTextarea.value = articleData.content;    // Update the content textarea           
                    urlLinkInput.value = articleData.link_url;      // Update the URL link input field            
                    nameLinkInput.value = articleData.link_name;    // Update the name link input field       
                    
                    // Update the image 
                    let fileName = articleData.img;
                
                    // Clear or set image-related elements based on whether or not an image exists
                    imgDdl.src = "";
                    imgDdl.alt = "";
                    imgDdl.style.maxWidth = "";
                    imageLabel.textContent= "";
                    
                    if (fileName) {
                        let fileExtension = fileName.split('.').pop().toLocaleLowerCase();
                        imgDdl.src = "./public/uploads/article_img/" + fileExtension + "/" + fileName;
                        imgDdl.alt = "Image actuelle";
                        imgDdl.style.maxWidth = "200px";
                        imageLabel.textContent= "Image actuelle";
                    } else {
                        imageLabel.textContent= "Aucune image liée actuellement à l'article";
                    }
                    
                })
                .catch(error => {
                    console.error(error);
                });
            });
        });        
    } catch(error) {
        console.error(error);  
    }
});


/***********************************************************************************/
/* *********************************** FUNCTION *************************************/
/***********************************************************************************/

/**  
 * formats a number as a string with leading zero if it's less than 10 
 * @param {number} number - the number to format.
 * @returns {string} - the formatted number as a string
*/
function formattedNumber (number) {
    if (number < 10) {
        return `0${number}`;
    } else {
        return number.toString();
    }
}

/**  
 * Udpate the accessibility setting (based on police, lineheight and contrast checkboxes) by adding or removing CSS classes.
 * @param {void}
 * @returns {void}
*/
function updateAccessibility() {

    // Check the state of the "policeCheckbox"
    if (policeCheckbox.checked === true) {
        cssData.fsDefault = "2.0rem";
        cssData.lhDefault = "initial";
    } else {
        cssData.fsDefault = "1.6rem";
        cssData.lhDefault = "2.0rem";
    }

    // Check the state of the "lineheightCheckbox"
    if (lineheightCheckbox.checked === true) {
        cssData.lhDefault = "3.0rem";
    } else {
        cssData.lhDefault = "2.0rem";
    }
    
    // Check the state of the "contrastCheckbox"
    if (contrastCheckbox.checked === true) {
        cssData.backgroundColor = "#333333";
        cssData.color = "#ffffff";
        cssData.titleColor = "#ffffff";
        cssData.imgBorderRadius = "0.5em";
        cssData.imgBackground = "#ffffff";
        cssData.linkColor = "#ffffff";
        gradientStops.forEach(stop => {
            stop.setAttribute("stop-color", cssData.color);
        });
        textElements.forEach(text => {
            text.style.fill = cssData.color;
        });
    } else {
        cssData.backgroundColor = "#ffffff";
        cssData.color = "#333333";
        cssData.titleColor = "#333333";
        cssData.imgBackground = "none";
        cssData.imgBorderRadius = "none";
        cssData.linkColor = "none";
        gradientStops.forEach(stop => {
            stop.setAttribute("stop-color", cssData.color);
        });
        textElements.forEach(text => {
            text.style.fill = cssData.color;
        });
    }
        localStorage.setItem('accessibilityPreferences', JSON.stringify(cssData));
        updateAccessibilityOnWebsite();
    }

/**  
 * Updates the website's CSS with new accessibility settings.
 * @param {void}
 * @returns {void}
*/
function updateAccessibilityOnWebsite() {
    // Generate new CSS based on the current accessibility settings
    let newCSS = `
        body {
            font-size: ${cssData.fsDefault};
            line-height: ${cssData.lhDefault};
            background-color: ${cssData.backgroundColor};
            color: ${cssData.color};
        }
        h3 {
            color: ${cssData.titleColor}; 
        }
        img.float {
        background-color: ${cssData.imgBackground};
        border-radius: ${cssData.imgBorderRadius};
        }
        a {
            color: ${cssData.linkColor}; 
        }
        span {
            font-size: ${cssData.fsDefault};
        }
        main #legendContainer, main #eventsResult {
            background-color: ${cssData.backgroundColor};
        }
        `;
    
    // Check if an existing style tag with ID "accessibilityStyle" exists and remove it
    let existingStyleTag = document.getElementById("accessibilityStyle");
    if (existingStyleTag) {
        document.head.removeChild(existingStyleTag);
    }
    
    // Create a new style tag with the updated CSS and add it to the document head
    let styleTag = document.createElement('style');
    styleTag.id = "accessibilityStyle";
    styleTag.innerHTML = newCSS;
    document.head.appendChild(styleTag);
}


/**
 * Toggles the visibility of a container based on the state of a target element.
 *
 * @param {HTMLElement} TargetElement - The element that triggers the visibility change.
 * @param {HTMLElement} TargetContainer - The element whose visibility will be toggled.
 * returns {void}
 */
function toggleVisibility(TargetElement, TargetContainer) {
    // Check the current state of the "aria-expanded" attribute of the target element (to determine if the container is open or close)
    let isOpen = TargetElement.getAttribute("aria-expanded") === "false";
    
    // Toggle the value of the "aria-expanded" attribute to switch between open and closed.
    TargetElement.setAttribute("aria-expanded", isOpen);
    
    // Conditions to open and close by add style
    if (isOpen) {
        TargetContainer.style.display = "block";
    } else {
        TargetContainer.style.display = 'none';
    }
}

/**  
 * Toggles the accessibility menu by adding or removing CSS classes. 
 * @param {void}
 * @returns {void}
*/
function toggleAccessibilityMenu() {
    // Toggle the 'active' class on the accessibility menu element, which controls its visibility
    selectAccessibilityMenu.classList.toggle('active');
    
    // Toggle the 'animate-chevron' class on the chevron element to animate it
    chevron.classList.toggle('animate-chevron');
}

/**  
 * Toggles the confirmation modal
 * @param {HTMLFormElement} form - The form associated with the delete action.
 * @returns {void}
*/
function openModal(form) {
    // Display the confirmation modal as a flex container
    confirmationModal.style.display = "flex";
    
    // Add a click event listener to the "Cancel" button
    cancelDeleteInput.addEventListener("click", (e) => {
        e.preventDefault();
        // Hide the confirmation modal
        confirmationModal.style.display = "none";
    });
            
    // Add a click event listener to the "Confirm" button
    confirmDeleteInput.addEventListener("click", (e) => {
    e.preventDefault();
    // Submit the form associated with the delete action            
    form.submit();
    // Hide the confirmation modal
    confirmationModal.style.display = "none";
    });
}

/**  
 * Updates the visibility and animation of map markers based on checkbox states.
 * @param {void}
 * @returns {void}
*/
function updateMarkersOnMap() {
    mapMarkers.forEach(function(marker) {
        let hasFlashClass = marker.options.className === 'flash-icon';
                
        if (last24Checkbox.checked === true && !hasFlashClass) {
            marker.setOpacity(0);
        } else if ((earthquakeCheckbox.checked === true && marker.type !== 'earthquake') || (otherEventsCheckbox.checked === true && marker.type === "other event")) {
            marker.setOpacity(0);
            marker.getElement().style.animation = 'none';
        } else {
            marker.setOpacity(marker.customOpacity);
            marker.getElement().style.animation = '';
        }
    });
}

/**  
 * Calculte Radius to draw earthquake icon
 * @param {number} magnitude - The magnitude of the earthquake event
 * @returns {object} - an object containing radius values for layers earthquake icon
*/
function calculateRadius(magnitude) {
    let baseRadius = 2.5 * Math.pow(2, magnitude);
    
    return {
        rayon1: baseRadius,
        rayon2: baseRadius * 0.75,
        rayon3: baseRadius * 0.5,
        rayon4: baseRadius * 0.25
    };
}

/**  
 * Generate earthquake icon depending of magnitude
 * @param {number} magnitude - The magnitude of the earthquake event
 * @returns {object} - A Leaflet divIcon object representing the custom icon.
*/

function generateEarthquakeIcon(magnitude) {
    
    let radius = calculateRadius(magnitude);
    let colorEvent = '' ;
    let iconSvg = '';
    
    // Determine the color and SVG markup for earthquake icons based on magnitude
    if (!isNaN(magnitude)) {    
        if (magnitude < 1) {
            colorEvent = '#FFBA08'; 
            iconSvg = `<circle cx="24" cy="24" r="${radius.rayon1}" fill="${colorEvent}" stroke="black" stroke-width="1" />
                      `;  
                            
        } else if (magnitude >= 1  && magnitude < 2) {
            colorEvent = '#E85D04'; 
            iconSvg = `<circle cx="24" cy="24" r="${radius.rayon1}" fill="${colorEvent}" stroke="black" stroke-width="1"/>
                       <circle cx="24" cy="24" r="${radius.rayon2}" fill="${colorEvent}" stroke="black" stroke-width="1" />
                       `;  
                            
        } else if (magnitude >= 2 && magnitude < 4) {
            colorEvent = '#D00000'; 
            iconSvg = `<circle cx="24" cy="24" r="${radius.rayon1}" fill="${colorEvent}" stroke="black" stroke-width="1"/>
                       <circle cx="24" cy="24" r="${radius.rayon2}" fill="${colorEvent}" stroke="black" stroke-width="1"/>
                       <circle cx="24" cy="24" r="${radius.rayon3}" fill="${colorEvent}" stroke="black" stroke-width="1" />
                       `;  
                            
        } else if (magnitude > 4) {
            colorEvent = '#6A040F'; 
            iconSvg = `<circle cx="24" cy="24" r="${radius.rayon1}" fill="${colorEvent}" stroke="black" stroke-width="1"/>
                       <circle cx="24" cy="24" r="${radius.rayon2}" fill="${colorEvent}" stroke="black" stroke-width="1"/>
                       <circle cx="24" cy="24" r="${radius.rayon3}" fill="${colorEvent}" stroke="black" stroke-width="1"/>
                       <circle cx="24" cy="24" r="${radius.rayon4}" fill="${colorEvent}" stroke="black" stroke-width="1" />
                       `;  
        }
    }
    
    // Create a Leaflet divIcon with the specified HTML and icon anchor. 
    return L.divIcon({
        className: '',
        html: `<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48">${iconSvg}</svg>`,
        iconAnchor: [22, 22]
    });
}

/**  
 * Generate other events icons depending of type
 * @param {string} eventType - The type of the event.
 * @returns {object} - A Leaflet divIcon object representing the custom icon
*/

function generateOtherCustomIcon(evenType) {
    let iconSvg = '';
    let type = '';
    
    if (evenType === "quarry blast") {
        type = "Tir de carrière";
        // iconUrl = `./public/img/icon/quarryblast.svg`;
        iconSvg = `<polygon fill="#FFEFC7" stroke="#000000" stroke-width="0.5" points="18.844,13.705 22.483,17.344 
        		    26.422,11.684 27.736,16.359 33.396,14.515 30.165,21.606 35.416,21.606 30.973,24.912 34.608,30.543 29.355,28.975 
        		    28.546,35.128 25.313,30.588 21.267,36.157 20.055,30.588 14.803,33.91 18.036,28.975 13.184,27.447 16.824,24.912 
        		    12.376,18.154 19.246,19.766"/>
        			<polygon fill="#FFEFC7" stroke="#000000" stroke-width="0.5" points="21.595,19.2 23.253,20.882 25.055,18.272 25.653,20.43 
        		    28.235,19.574 26.757,22.851 29.155,22.851 27.125,24.379 28.789,26.977 26.389,26.254 26.021,29.094 24.544,27.001 22.698,29.568 
        		    22.15,27.001 19.75,28.534 21.228,26.254 19.014,25.551 20.673,24.379 18.647,21.255 21.782,22.002"/>
        			`;  
    } else if (evenType === "rockslide") {
        type = "Chute de bloc/éboulement";
        // iconUrl = `./public/img/icon/rockslide.svg`; 
        iconSvg = `<polygon fill="#C7C9FF" stroke="#000000" stroke-width="0.5" points="17.043,15.634 11.498,24.502 12.506,29.967 19.18,34.666 27.691,30.41 29.949,22.141 25.146,14.954 "/>
        		    <polygon fill="#C7C9FF" stroke="#000000" stroke-width="0.5" points="30.715,25.004 32.627,27.975 35.08,27.975 36.486,25.078 35.08,22.382 31.572,23.08 				"/>
        			<polygon fill="#C7C9FF" stroke="#000000" stroke-width="0.5" points="31.803,30.549 29.668,30.1 28.221,32.328 29.662,33.693 31.936,34.096 32.518,32.092 "/>
        		    `; 
    } else if (evenType === "other event") {
        type = "Autre évènement";
        // iconUrl = `./public/img/icon/otherevent.svg`;
        iconSvg = `<polygon fill="#DBDBDB" stroke="#000000" stroke-width="0.5" points="16.709,24 23.963,37.6 31.291,24 24.113,10.324"/>
        			`; 
    } else if (evenType === "landslide") {
        type = "Glissement de terrain";
        // iconUrl = `./public/img/icon/landslide.svg`;
        iconSvg = `<polygon fill="#C7EDFF" stroke="#000000" stroke-width="0.5" points="9.889,15.866 19.256,32.381 37.928,32.381"/>
                    `;  
    } 
    
    return {
        icon: L.divIcon({
        className: '',
        html: `<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48">${iconSvg}</svg>`,    
        //html: `<img src="${iconUrl}" width="32" height="32" />`,
        iconAnchor: [16, 16]
        }),
        type: type 
    };
}


                        
                       