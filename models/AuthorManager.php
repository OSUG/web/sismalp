<?php

declare(strict_types=1);

require_once './models/Manager.php';

class AuthorManager extends Manager {
    
    /* Insert a new Author in database */
    public function insert(Author $author): ?string {
            
        // Prepare data for insertion    
        $parameters = [
            'name'     => $author->getName(),
            'login'    => $author->getLogin(),
            'email'    => $author->getEmail(),
            'password' => password_hash($author->getPassword(), PASSWORD_BCRYPT),
            'role'     => $author->getRole(),
        ];
        
        // Create the insertion query
        $query = $this->db->prepare("INSERT INTO authors(name, login, email, password, role) VALUES (:name, :login, :email, :password, :role)");
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
        
        // Return last insert Id
        return $this->db->lastInsertId();
    }
    
    /* Select an Author in database */   
    public function isExisting(Author $author): array|bool {
        
        // Prepare parameters for query   
        $parameters = [
            "email" => $author->getEmail(),
            "login" => $author->getLogin(),
        ];
        
        // Create the select query
        $query = $this->db->prepare("
        SELECT *
        FROM `authors` 
        WHERE email= :email OR login= :login 
        limit 1");
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
        
        // Fetch author if existing and return it
        $author = $query->fetch();
        return $author;
    }
    
    /* Select an Author by Id in database */   
    public function selectOneById(int $id): array|bool {
        
        // capture the user's session ID if existing
        if(isset($_SESSION['id'])) { 
            $idConnect = $_SESSION['id'];
        }
        
        // Prepare parameters for query   
        $parameters = [
            'id' => $idConnect    
        ];
        
        // Create the select query
        $query = $this->db->prepare("SELECT * FROM authors WHERE authors.id = :id limit 1");
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
        
        // Fetch author if existing and return it
        $idConnect = $query->fetch();
        return $idConnect;
    }
        
    
    /* Select all authors from the database */
    public function selectAll(): array {
    
    // Create a query to authors  
    $query = $this->db->prepare('SELECT * FROM authors ORDER BY id');
    
    // Execute the query 
    $query->execute();
    
    // Fetch all authors as an array and return it
    $authors = $query->fetchAll();
    
    return $authors;
    } 
        
    
    /* Update Author by Id in database */   
    function update(Author $author): void {
        
        // capture the user's session ID if existing
        if(isset($_SESSION['id'])) {
        $idConnect = $_SESSION['id'];
        }
        
        // Prepare parameters for query   
        $parameters = [
            'id'        => $idConnect,
            'name'      => $author->getName(),
            'login'     => $author->getLogin(),
            'email'     => $author->getEmail(),
            'password'  => password_hash($author->getPassword(), PASSWORD_BCRYPT),
        ];
        
        // Create the select query
        $query = $this->db->prepare("
            UPDATE authors
            SET
                name = :name, 
                login = :login, 
                email = :email, 
                password = :password 
            WHERE authors.id = :id
        ");
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
    }
    
    /* Delete Author by Id in database */       
    function delete(Author $author): void {  
        
        // Prepare parameters for query      
        $parameters = [
            "id" => $author->getId(),
        ];
            
        // Create the delete query
        $query = $this->db->prepare(
        'DELETE FROM authors 
        WHERE `id`= :id'
        );
            
        // Execute the query with the prepared parameters
        $query->execute($parameters);    
    }
}
