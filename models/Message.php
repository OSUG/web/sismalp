<?php

declare(strict_types=1);

require_once './models/Model.php';

class Message extends Model {
    
    private ?string $email;
    private ?string $phone;
    private ?string $message;
    private ?DateTime $createdAt;
    private ?int $archived;
    private ?int $typeId;
    
    // message field minimum value
    const MIN_MESSAGE_LENGTH = 8;
    
    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(?string $email): void {
        $this->email = $email;
    }

    public function getPhone(): ?string {
        return $this->phone;
    }

    public function setPhone(?string $phone): void {
        $this->phone = $phone;
    }

    public function getMessage(): ?string {
        return $this->message;
    }

    public function setMessage(?string $message): void {
        $this->message = $message;
    }

    public function getCreatedAt(): ?DateTime {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTime $createdAt): void {
        $this->createdAt = $createdAt;
    }

    public function getArchived(): ?int {
        return $this->archived;
    }

    public function setArchived(?int $archived): void {
        $this->archived = $archived;
    }

    public function getTypeId(): ?int {
        return $this->typeId;
    }

    public function setTypeId(?int $typeId): void {
        $this->typeId = $typeId;
    }
    
}
