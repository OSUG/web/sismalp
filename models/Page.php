<?php

declare(strict_types=1);

require_once './models/Model.php';

class Page extends Model {
    
        private ?string $name;
        
        // name field maximum value
        const MAX_NAME_LENGTH = 30;

        public function getName(): ?string {
        return $this->name;
        }
    
        public function setName(?string $name): void {
            $this->name = $name;
        }
}
