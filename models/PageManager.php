<?php

declare(strict_types=1);

require_once './models/Manager.php';

class PageManager extends Manager {

    /* Insert a new Page into the database */
    public function insert(Page $page): void {
        
        // Prepare parameters for insertion    
        $parameters = [
         "name" => $page->getName(),
         ];
        
        // Create the insertion query
        $query = $this->db->prepare("INSERT INTO pages(name) VALUES (:name)");
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
    }
    
    /* Select all pages from the database */
        public function selectAll(): array {
        
        // Create a query to fetch pages 
        $query = $this->db->prepare('SELECT * FROM pages ORDER BY name');
        
        // Execute the query 
        $query->execute();
        
        // Fetch all pages as an array and return it
        $pages = $query->fetchAll();
        
        return $pages;
    }

}
