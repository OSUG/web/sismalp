<?php

declare(strict_types=1);

require_once './models/Model.php';

class Contact extends Model {
    
        private ?string $type;
        
        // name field maximum value
        const MAX_NAME_LENGTH = 30;

        public function getType(): ?string {
        return $this->type;
        }
    
        public function setType(?string $type): void {
            $this->type = $type;
        }
}
