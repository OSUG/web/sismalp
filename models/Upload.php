<?php

class Upload {
    
    private ?string $fileName;
    private ?string $fileSize; 
    private ?string $fileTemporyLocation; 
    private ?string $fileTargetDirectory; 
    private ?int $maxFileSize;  
    private ?array $allowedExtensions;
    private ?array $allowedMimeTypes;
    public ?array $fileErrors = [];
     
    // Constructor to initialize properties with default values 
    public function __construct(
        $maxFileSize = 2*1024*1024, 
        $allowedExtensions = ['jpeg', 'jpg', 'png', 'gif'], 
        $allowedMimeTypes = ['image/jpeg', 'image/png', 'image/gif'], 
        $fileTargetDirectory = "public/uploads/article_img",
    	)
    {  
        $this->maxFileSize = $maxFileSize;
        $this->allowedExtensions = $allowedExtensions;
        $this->allowedMimeTypes = $allowedMimeTypes;
        $this->fileTargetDirectory = $fileTargetDirectory;
    }
	
	/** 
	 * Method to validate, rename, and upload a file.
	 * 
	 * @param string $fileName : Name of the uploaded file.
	 * @param int $fileSize : size of the uploaded file.
	 * @param string $fileTemporyLocation : temporary location of the uploaded file.
	 * 
	 * @return string|array : the new file name if successful, ohtherwise false.
	 */
	public function validateRenameAndUploadFile(string $fileName, int $fileSize, string $fileTemporyLocation): string|array {
		$this->fileName = $fileName;
        $this->fileSize = $fileSize;
        $this->fileTemporyLocation = $fileTemporyLocation;

		// Validating the file size
		if(intval($this->fileSize) > $this->maxFileSize){
	      $this->fileErrors[] = "taille de fichier non autorisée (max 2MB)";
		}
		
		// Validating the file type 
		$fileExtension = pathinfo($this->fileName, PATHINFO_EXTENSION);
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime = finfo_file($finfo, $this->fileTemporyLocation);
		finfo_close($finfo);
		
		if (!in_array(strtolower($fileExtension), $this->allowedExtensions) || !in_array($mime, $this->allowedMimeTypes)) {
		    $this->fileErrors[] = "type de fichier non autorisé";
		}
		
		// If no errors, rename and upload file
		if (empty($this->fileErrors)) {
			$fileNameWithoutExtension = pathinfo($this->fileName, PATHINFO_FILENAME);
			$newFileName = $fileNameWithoutExtension . '-' . uniqid() . '.' . $fileExtension;
	   		$targetDirectory = $this->fileTargetDirectory . '/' . $fileExtension;
	   		$targetPath =  $targetDirectory . '/' . $newFileName;
	   		
	    	if(!is_dir($targetDirectory)) {
                mkdir($targetDirectory);
            }

	   		if (move_uploaded_file($this->fileTemporyLocation, $targetPath)) {
        		return $newFileName;
    		} 
        } else {
        	return $this->fileErrors;
    	}
	}
}    




		