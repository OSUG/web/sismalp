<?php

declare(strict_types=1);

require_once './models/Model.php';

class Article extends Model {
    
        private ?string $title;
        private ?string $content;
        private ?string $img;
        private ?string $linkName;
        private ?string $linkUrl;
        private ?DateTime $createdAt;
        private ?DateTime $modifiedOn;
        private ?int $pageId;
        private ?int $authorId;
        
        // public function __construct(?string $title, ?string $content, ?string $img, ?string $linkUrl, ?DateTime $createdAt, ?int $pageId, ?int $authorId) {
        // $this->title = $title;
        // $this->content = $content;
        // $this->img = $img;
        // $this->linkUrl = $linkUrl;
        // $this->createdAt = $createdAt;
        // $this->modifiedOn = $modifiedOn;
        // $this->pageId = $pageId;
        // $this->authorId = $authorId;
        // }
        

        public function getTitle(): ?string {
        return $this->title;
        }
    
        public function setTitle(?string $title): void {
            $this->title = $title;
        }
        
        public function getContent(): ?string {
        return $this->content;
        }
    
        public function setContent(?string $content): void {
            $this->content = $content;
        }
        
        public function getImg(): ?string {
        return $this->img;
        }
    
        public function setImg(?string $img): void {
            $this->img = $img;
        }
        
        public function getLinkName(): ?string {
        return $this->linkName;
        }
    
        public function setLinkName(?string $linkName): void {
            $this->linkName = $linkName;
        }
        
        public function getLinkUrl(): ?string {
        return $this->linkUrl;
        }
    
        public function setLinkUrl(?string $linkUrl): void {
            $this->linkUrl = $linkUrl;
        }
        
        public function getCreatedAt(): ?DateTime {
        return $this->createdAt;
        }
    
        public function setCreatedAt(?DateTime $createdAt): void {
            $this->createdAt = $createdAt;
        }
        
        public function getModifiedOn(): ?DateTime {
        return $this->modifiedOn;
        }
    
        public function setModifiedOn(?DateTime $modifiedOn): void {
            $this->modifiedOn = $modifiedOn;
        }
        
        public function getPageId(): ?int {
        return $this->pageId;
        }
    
        public function setPageId(?int $pageId): void {
            $this->pageId = $pageId;
        }
        
        public function getAuthorId(): ?int {
        return $this->authorId;
        }
    
        public function setAuthorId(?int $authorId): void {
            $this->authorId = $authorId;
        }
        
}
