<?php

declare(strict_types=1);

require_once './models/Manager.php';

class ArticleManager extends Manager {
    
    /* Insert a new Article in database */
    public function insert(Article $article): void {
        
        // Prepare data for insertion    
        $parameters = [
            'title'     => $article->getTitle(),
            'content'   => $article->getContent(),
            'img'       => $article->getImg(),
            'link_name' => $article->getLinkName(),
            'link_url'  => $article->getLinkUrl(),
            'page_id'   => $article->getPageId(),
            'author_id' => $article->getAuthorId(),
        ];
        
        // Create the insertion query
        $query = $this->db->prepare("
            INSERT INTO articles (
                title,
                content,
                img,
                link_name,
                link_url,
                page_id,
                author_id
            ) VALUES (
                :title,
                :content,
                :img,
                :link_name,
                :link_url,
                :page_id,
                :author_id
            )
        ");
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
    }
    
    /* Select all articles by Page from the database */
    public function selectByPages($page): array {
        
        // Prepare parameter for query    
        $parameters = [
             "pageName" => '%' . $page . '%',
        ];
        
        // Create a query to fetch articles by page
        $query = $this->db->prepare('
        SELECT articles.title, articles.content, articles.img, articles.link_name, articles.link_url, articles.created_at, articles.modified_on, pages.name, authors.login 
        FROM articles 
        INNER JOIN pages ON articles.page_id = pages.id
        INNER JOIN authors ON articles.author_id = authors.id
        WHERE pages.name LIKE :pageName
        ORDER BY articles.id
        ');
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
    
        // Fetch all articles as an array and return it
        $articles = $query->fetchAll();
    
        return $articles;
    }
    
    /* Select one article from the database */
    public function selectOneById($articleId): array {
        
        $parameters = [
             "articleId" => $articleId,
        ];
        
        $query = $this->db->prepare("
        SELECT articles.id, articles.title, articles.content, articles.img, articles.link_name, articles.link_url, articles.created_at, articles.modified_on, pages.name AS page, authors.name AS author 
        FROM articles 
        INNER JOIN pages ON articles.page_id = pages.id
        INNER JOIN authors ON articles.author_id = authors.id
        WHERE articles.id = :articleId
        ");
        
        // Execute the query 
        $query->execute($parameters);
        
        $article = $query->fetch();
        
        return $article;
    }
    
    /* Select all articles from the database */
    public function selectAll(): ?array {
        
        // Create a query to fetch articles along with contact types
        $query = $this->db->prepare('
        SELECT articles.id, articles.title, articles.content, articles.img, articles.link_name, articles.link_url, articles.created_at, articles.modified_on, pages.name AS page, authors.name AS author 
        FROM articles 
        INNER JOIN pages ON articles.page_id = pages.id
        INNER JOIN authors ON articles.author_id = authors.id
        ORDER BY articles.id 
        ');
        
        // Execute the query 
        $query->execute();
        
        // Fetch all articles as an array and return it
        $articles = $query->fetchAll();
    
        return $articles;
    }
    
    /* Update an Article in database */
    public function update(Article $article): void {
        
        // Prepare data for insertion    
        $parameters = [
            'id'        => $article->getId(),
            'title'     => $article->getTitle(),
            'content'   => $article->getContent(),
            'img'       => $article->getImg(),
            'link_name' => $article->getLinkName(),
            'link_url'  => $article->getLinkUrl(),
            'page_id'   => $article->getPageId(),
            'author_id' => $article->getAuthorId(),
        ];
        
        // Create the update query
        $query = $this->db->prepare("
            UPDATE articles 
            SET title = :title,
                content = :content,
                img = :img,
                link_name = :link_name,
                link_url = :link_url,
                page_id = :page_id,
                author_id = :author_id
            WHERE `id`= :id
        ");
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
    }
    
    /* Delete an article */
    public function delete(Article $article): void {
        
        // Prepare parameters for delete query
        $parameters = [
             "articleId" => $article->getId(),
        ];
        
        // Create a delete query to remove the message
        $query = $this->db->prepare(
        'DELETE FROM articles 
        WHERE `id`= :articleId'
        );
        
        // Execute the query with the prepared parameter
        $query->execute($parameters);
    }
    
}
