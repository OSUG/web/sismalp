<?php

declare(strict_types=1);

require_once './models/Manager.php';

class ContactManager extends Manager {

    /* Insert a new Contact type into the database */
    public function insert(Contact $contact): void {
        
        // Prepare parameters for insertion    
        $parameters = [
         "type" => $contact->getType(),
         ];
         
        // Create the insertion query
        $query = $this->db->prepare("INSERT INTO contacts(type) VALUES (:type)");
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
    }
    
    /* Select all contacts type from the database */
    public function selectAll(): array {
    
    // Create a query to contacts type 
    $query = $this->db->prepare('SELECT * FROM contacts ORDER BY type');
    
    // Execute the query 
    $query->execute();
    
    // Fetch all contacts type as an array and return it
    $contacts = $query->fetchAll();
    
    return $contacts;
    }

}