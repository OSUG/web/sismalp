<?php

declare(strict_types=1);

require_once './models/Model.php';

class Author extends Model {
    
    private ?string $name;
    private ?string $login;
    private ?string $email;
    private ?string $password;
    private ?int $role;
    
    // public function __construct(?string $login, ?string $email, ?string $password, ?int $role) {
    //     $this->login = $login;
    //     $this->email = $email;
    //     $this->password = $password;
    //     $this->role = $role;
    // }
    
    // name field maximum value
    const MAX_LOGIN_LENGTH = 20;
    const MIN_PASSWORD_LENGTH = 8;
    const MAX_PASSWORD_LENGTH = 128;
    
    public function getName(): ?string {
        return $this->name;
    }
    
    public function setName(?string $name): void {
        $this->name = $name;
    }
    
    public function getLogin(): ?string {
        return $this->login;
    }
    
    public function setLogin(?string $login): void {
        $this->login = $login;
    }
    
    public function getPassword(): ?string {
        return $this->password;
    }
    
    public function setPassword(?string $password): void {
        $this->password = $password;
    }
    
    public function getEmail(): ?string {
        return $this->email;
    }
    
    public function setEmail(?string $email): void {
        $this->email = $email;
    }
    
    public function getRole(): ?int {
        return $this->role;
    }
    
    public function setRole(?int $role): void {
        $this->role = $role;
    }
    

    public function validatePassword(?string $password): bool|array {
        $errorsPassword = [];
        if (strlen(trim($password)) < self::MIN_PASSWORD_LENGTH) {
            $errorsPassword[] = "Le mot de passe doit contenir minimum 8 caractères.";
        }
        if (strlen(trim($password)) > self::MAX_PASSWORD_LENGTH) {
            $errorsPassword[] = "Le mot de passe ne peut pas dépasser 128 caractères.";
        }
        if (!preg_match('/[A-Z]/', $password)) {
            $errorsPassword[] = "Le mot de passe doit contenir au moins une lettre majuscule.";
        }
        if (!preg_match('/[a-z]/', $password)) {
            $errorsPassword[] = "Le mot de passe doit contenir au moins une lettre minuscule.";
        }
        if (!preg_match('/[0-9]/', $password)) {
            $errorsPassword[] = "Le mot de passe doit contenir au moins un chiffre.";
        }
        if (!preg_match('/[!@#$%^&*()\-_=+{};:,<.>]/', $password)) {
            $errorsPassword[] = "Le mot de passe doit contenir au moins un caractère spécial.";
        }
        if (count($errorsPassword) === 0) {
            return true; 
        } else {
            return $errorsPassword;
        }
    }
    
}