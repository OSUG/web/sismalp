<?php

declare(strict_types=1);

require_once './models/Manager.php';

class MessageManager extends Manager {
    
    /* Insert a new Message into the database */
    public function insert(Message $message): void {
        
        // Prepare data for insertion    
        $parameters = [
            'email'     => $message->getEmail(),
            'phone'     => $message->getPhone(),
            'message'   => $message->getMessage(),
            'archived'  => $message->getArchived(),
            'type_id'   => $message->getTypeId(),
        ];
        
        // Create the insertion query
        $query = $this->db->prepare("
            INSERT INTO messages (
                email,
                phone,
                message,
                archived,
                type_id
            ) VALUES (
                :email,
                :phone,
                :message,
                :archived,
                :type_id
            )
        ");
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
    }
    
    /* Select all messages from the database */
    public function selectAll(): ?array {
        
        // Create a query to fetch messages along with contact types
        $query = $this->db->prepare('
        SELECT messages.id, messages.email, messages.phone, messages.message, messages.created_at, messages.archived, contacts.type
        FROM messages 
        INNER JOIN contacts ON messages.type_id = contacts.id
        ORDER BY created_at DESC
        ');
        
        // Execute the query 
        $query->execute();
        
        // Fetch all messages as an array and return it
        $messages = $query->fetchAll();
    
        return $messages;
    }
    
    /* Update Archive status of message */
    public function archive(Message $message): void {
        
        // Prepare parameters for update query
        $parameters = [
             "messageId" => $message->getId(),
             "archived" => $message->getArchived()
        ];
        
        // Create an update query to set archived status
        $query = $this->db->prepare(
        'UPDATE messages
        SET archived = :archived
        WHERE `id`= :messageId'
        );
        
        // Execute the query with the prepared parameters
        $query->execute($parameters);
    }
    
    /* Delete a message */
    public function delete(Message $message): void {
        
        // Prepare parameters for delete query
        $parameters = [
             "messageId" => $message->getId(),
        ];
        
        // Create a delete query to remove the message
        $query = $this->db->prepare(
        'DELETE FROM messages 
        WHERE `id`= :messageId'
        );
        
        // Execute the query with the prepared parameter
        $query->execute($parameters);
    }
    
    
}
