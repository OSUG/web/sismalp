Avancée du projet

Semaine 1 (4 août 2023)
J'ai démarré mon projet par l'interface d'administration :
- La BDD a été créée.
- Le modèle MCV mis en place
- Les configurations d'accès sont fonctionnelles (db_config, router et routes)
- J'ai commencé la mise en place du système d'authentification : l'enregistrement des auteurs, login, logout, fonctionne
> il me reste à sécuriser plus fortement les données entrées pour enregistrement
(vérifications de validité des mails, et des mots de passes) : j'avais débuté des choses, mais j'ai mis de côté pour le moment
> il faut que j'implémente une fonction delete pour le superadmin, je m'en occuperai plus tard
- J'ai mis en place l'ajout des pages.
> il faut que j'implémente une fonction delete pour le superadmin, je m'en occuperai plus tard
- Je suis en train de mettre en place l'ajout des articles (class et manager créé), j'en suis au contrôleur
Semaine 2 (11 août 2023)

Sécurisation des emails et des mots de passes effectués (prête pour un test !)
Ajout des articles terminé avec une fonction upload fonctionnelle
Ajout des types de contact pour le formulaire de contact
Ajout de la classe Message et de son manager
Finalisation en cours du controleur de message
Semaine pro -> mise en forme du back et des premières pages du front

Semaine 3 (17 août 2023)

Mise en forme du frontend
Debut du css realisation du header et footer
Réalisation du menu burger
Semaine pro -> mise en forme du back et débuts des requetes

Semaine 4 (25 août 2023)

Affichage des données sur le back pour la partie de gestion des messages
Affichage de la gestion des articles en cours de finalisation
Réalisation d'une modale en js pour la suppression des données
Ajout de token pour prévenir la faille csrf
Harmonisation d'écriture, type hinting et vérification de developpement DRY
Test de requête AJAX en JS puis avec controlleur PHP pour les événements
semaine pro -> finalisation du back

Semaine 5 (1 septembre 2023)

Vérification du htmlspecialchars
Ajout de la fonction delete sur les articles et affichage sur la vue pour la gestion des articles
Réalisation d'une requête AJAX en JS pour la mise à jour des articles

Semaine 6-7

Reprise du travail, pas de temps dégagé pour travailler sur le site

Semaine 8 (24 septembre 2023)

Finalisation de la gestion des auteurs et avancée du css du backoffice
Finalisation du js pour la mise à jour automatique des articles, manque une petite une partie d'affichage js
Finalisation du controller de gestion des articles
-> semaine pro -> essais d'importation pour préparer la démonstration au commanditaire

Semaine 9 (1 octobre 2023)

Correction des bugs de css
Travail en JS sur l'importation des points sur la carte pour démonstration
Plusieurs essais de carte infructueux (problème avec la requête AJAX en JS, finalement résolu)
-> semaine pro -> travail sur la carte

Semaine 10 (6 octobre 2023)

Rdv avec le commanditaire pour présentation de l'état d'avancement
Création du logo Sismalp
Création des icons svg et mise à jour des points sur la carte
Création de la légende
-> semaine pro -> travail sur la carte

Semaine 11 (15 octobre 2023)

Gestion de l'interactivité de la carte en JS et affichage en CSS :

mise en place d'un dégradé (opacité) en fonction d'âge des événements,
mise en place d'un clignotement pour les événements de moins de 24h,
variation de la taille des événements sismiques en fonction de la magnitude


création de filtres interactifs pour gérer l'affichage
création d'un tableau dynamique des derniers événements
-> semaine pro -> travail sur l'accessibilité

Semaine 12 (21 octobre 2023)

Réalisation d'un menu d'accessibilité
Réalisation d'une page 404 et test de navigation pour vérification des erreurs
Finalisation et réorganisation du JS pour respecter un développement DRY et type hinting
Envoi du projet à la 3WA
-> transfert sur nos serveurs et démarrage des tests utilisateurs