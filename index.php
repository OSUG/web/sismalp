<?php

date_default_timezone_set('Europe/Paris');
setlocale(LC_TIME, 'fr_FR.UTF-8');

session_start();

require_once './configs/Database.php';
require_once './configs/Router.php';
require_once './configs/routes.php';

require_once './controllers/HomeController.php';
require_once './controllers/AuthorController.php';
require_once './controllers/PageController.php';
require_once './controllers/ArticleController.php';
require_once './controllers/ContactController.php';
require_once './controllers/MessageController.php';
require_once './controllers/EventController.php';
require_once './controllers/ErrorController.php';


$router = new Router();

if(!$router->isAjax()) {
    require_once './views/layout.phtml';
} else {
    $router->callActionFromRequest();
}

