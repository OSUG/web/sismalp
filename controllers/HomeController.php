<?php

declare(strict_types=1);

require_once './models/PageManager.php';
require_once './models/Page.php';

class HomeController {
    
    public function home(): void {
        require './views/frontend/home.phtml';
    }
    
    public function legalesMentions(): void {
        require './views/frontend/mentions_legales.phtml';
    }
    
}

