<?php

declare(strict_types=1);

require_once './models/Contact.php';
require_once './models/ContactManager.php';
require_once './models/MessageManager.php';
require_once './models/Message.php';
// require_once './iconcaptcha/vendor/autoload.php';

use IconCaptcha\IconCaptcha;

class MessageController {
    
    /**
     * Add a new message through the public contact form.
     * This function insert message submit through the public contact form into the database 
     *
     * @param void
     * 
     * @return void
     */
    public function add(): void {
        
        try {
            $messageManager = new MessageManager();  //Instantiate a MessageManager
            $contactManager = new ContactManager(); // Instantiate a ContactManager and retrieve all type of contacts from the database
            $allContacts = $contactManager->selectAll();
            $errors = []; // Table for storing errors
            
            if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                
                $message = new Message;

                // Check the validation_anonyme field 
                if (!empty($_POST['validation_anonyme'])) {
                    // If the field is not empty, it's likely a bot
                    $errors[] = "Soumission invalide";
                }
                
                foreach($_POST as $key => $data) {
                    if($key != 'validation_anonyme' && empty(trim($data))) {
                        $errors[$key] = 'Merci de compléter un ou plusieurs champs.';
                    }
                }
               
                if (isset($_POST["email"]) && !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) { // Validate and set email if existing
                    $errors[] = "Email non valide.";
                } else {
                    $message->setEmail($_POST['email']);
                }
                
                if (isset($_POST["phone"])) {
    
                $phoneValidation = preg_replace('/[\s-]+/', '', $_POST["phone"]); // Validate and set phone number if existing
    
                    if (!preg_match("/^(\+\d{1,4})?[0-9]{7,12}$/", $phoneValidation)) {
                        $errors[] = "Numéro de téléphone incorrect.";
                    } else {
                        $message->setPhone($_POST['phone']);
                    }
                }
                
                if (isset($_POST['message']) && strlen($_POST['message']) < Message::MIN_MESSAGE_LENGTH) { // Validate and set content if existing
                    $errors[] = "Merci de détailler un peu plus votre demande.";
                } else {
                    $message->setMessage(trim($_POST['message']));
                }
                
                if (isset($_POST['type_id'])) {  // Validate and set type of contact if existing
                    $matchFound = false;
                    for($i = 0; $i < count($allContacts); $i++) {
                        if($_POST['type_id'] == $allContacts[$i]['id']) {
                            $matchFound = true;
                            $message->setTypeId((int)$_POST['type_id']);
                            break;
                        }
                    }
                }
                if(!$matchFound) {
                    $errors[] = "Erreur de sélection dans la liste déroulante";
                }

                // Load the IconCaptcha configuration.
                // THE DEFAULT CONFIGURATION FILE CAN BE FOUND IN THE /examples FOLDER.
                $config = require './iconcaptcha/captcha-config.php';
            
                // Create an instance of IconCaptcha.
                $captcha = new IconCaptcha($config);

                 // Validate the captcha.
                 $validation = $captcha->validate($_POST);
                  
                if (empty($errors)) { // if no errors set Archived 
                    $message->setArchived(null);
                    
                    if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                        $messageManager->insert($message); // Insert message in database

                        // Send email with form information
                        $to = 'mickael.langlais@univ-grenoble-alpes.fr'; // email recipient
                        $subject = 'SISMALP - Nouveau message reçu';
                        $messageBody = "Récapitulatif des informations soumises via le formulaire de contact SISMALP:" . "<br>";
                        $headers = "Content-Type: text/html; charset=UTF-8";

                        foreach ($_POST as $key => $value) {
                            // Exclude certain fields from email (e.g., token, validation_anonyme)
                            if ($key !== 'token' && $key !== 'validation_anonyme') {
                                $displayValue = $value;
                                if ($key === 'type_id' && isset($allContacts[$value])) {
                                    $key = "type";
                                    $displayValue = $allContacts[$value]['type']; 
                                }
                                $messageBody .= "<b>$key:</b> $displayValue<br>";
                            }
                        }

                        mail($to, $subject, $messageBody, $headers);

                        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                        header('Location: index.php?page=contact&message=ok'); // redirection after successful processing
                        exit();
                    } else {
                        $errors[] = "Une erreur est survenue au moment de la soumission du formulaire";  // Error invalid token
                    }
                    
                }
            
            }
            $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update a new token
            require './views/frontend/contact_form.phtml'; // Require the public contact form view 
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
    
    
    /**
     * Show messages in the backend for management.
     * This function retrieve and displays all messages and counts the number of non-archived messages
     *  
     * @param void
     * @return void
     */
    public function manage(): void {
        
        try {
            
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 2) {  // Check user right
                $messageManager = new MessageManager(); // Instantiate a MessageManager and retrieve all messages from the database
                $messages = $messageManager->selectAll();
                
                $countNotArchivedMessages = 0; // Count the number of non-archived messages
                foreach ($messages as $message) {
                    if ($message['archived'] === null) {
                        $countNotArchivedMessages++;
                    }
                }
            } else {
                header('Location: index.php?page=login');  // Redirect if user role is not valid
            }
            require './views/backend/manage_requests.phtml'; // Require the view to display messages and management options
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }  
    
    /**
     * Archive or unarchive a message.
     *  
     * @param int|null $archivedValue The value to set for archived status (1 or null)
     * @param string $action The action to perform (archive or unarchive)
     * 
     * @return void
     */
    public function archive(?int $archivedValue, string $action): void { 
        $errors = [];
        
        try {
            
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 2) { // Check user rigth 
                
                if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                    
                    if (isset($_POST["message_id"]) && !empty($_POST["message_id"])) { // Check if message ID is set and not empty
                        $message = new Message(); // Create a new Message object and set its ID and archived
                        $message->setId((int) $_POST['message_id']);
                        $message->setArchived($archivedValue);
        
                        if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                            $messageManager = new MessageManager(); // Instantiate a MessageManager and call the archive method
                            $messageManager->archive($message); // Udpate archive statut in database
                            $_SESSION['token'] = bin2hex(random_bytes(35));  // Generate and update after successful processing
                        } else {
                            $errors[] = "Une erreur s'est produite"; // Error invalid token
                        }
        
                        header('Location: index.php?page=manage_requests'); // Redirect after processing
                        exit();
                    }
                } else {
                    $errors[] = "Vous n'avez pas les droits pour modifier le statut des messages, veuillez contacter votre webmaster";
                }
            } else {
                header('Location: index.php?page=login'); // Redirect if user role is not valid
            }
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    
        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update token
    }
    
   /**
     * Toggle the archived status of a message.
     * 
     * @param void
     *
     * @return void
     */
    public function toggleArchive(): void { 
        
        try {
            $action = $_POST['action']; 
            if ($action === 'archive') {
                $this->archive(1, $action); // Call to archive public function with parameter 1
            } else {
                $this->archive(null, $action); // Call to unarchive public function with null parameter
            } 
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
    
    /**
     * Delete a message 
     *
     * @param void
     * 
     * @return void
     */
    public function delete(): void { 
        
        $errors = []; // Errors array for display
        
        try {
            
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 2) { // Check user rigth 
                
                if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                        
                        if (isset($_POST["message_delete"]) && !empty($_POST["message_delete"])) { // Target message based on POST data
                            $messageId = (int)$_POST['message_delete']; 
                        } else if (isset($_POST["message_delete_archived"]) && !empty($_POST["message_delete_archived"])) {
                            $messageId = (int)$_POST['message_delete_archived'];
                        } else {
                            $errors[] = "Une erreur s'est produite";  
                        }
                            
                            if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                                $message = new Message(); // Create a new Message object and set its ID
                                $message ->setId($messageId);
                                $messageManager = new MessageManager(); // Instantiate a MessageManager and call the delete method
                                $messageManager->delete($message); // Delete message in database
                                
                                $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                            } else {
                                $errors[] = "Une erreur s'est produite"; // Error invalid token
                            }
                        
                        header('Location: index.php?page=manage_requests'); // Redirect after successful processing
                        exit();
                } else {
                        $errors[] = "Vous n'avez pas les droits pour supprimer les messages, veuillez contact votre webmaster"; // Error invalid user rigth
                }
            } else {
                header('Location: index.php?page=login'); // Redirect if user role is not valid
            }
            $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update a new token
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
}
