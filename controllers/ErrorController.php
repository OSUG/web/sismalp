<?php

declare(strict_types=1);

class ErrorController {
    
    public function NotFoundPage(): void {
        require './views/404.phtml';
    }
    
}