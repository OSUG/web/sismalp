<?php

declare(strict_types=1);

require_once './models/PageManager.php';
require_once './models/Page.php';

class PageController {
    
    /**
     * Add a new page.
     * This function inserts a new page into the database for display on the public website.  
     *
     * @param void
     * @return void
     */
    public function add(): void {
        
        try {
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 1) { // Check user rigth 
                $pageManager = new PageManager(); // Instantiate a PageManager
                $errors = []; // Table for storing errors
                
                if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                    $page = new Page();
                    
                    if (isset($_POST['name']) && empty($_POST['name'])) { // Validate and set page
                        $errors [] = "Veuillez saisir un nom pour votre page";
                    } else if (!empty($_POST['name']) && strlen($_POST['name']) > Page::MAX_NAME_LENGTH) {
                        $errors[] = "Le nom de la page ne peut pas dépasser " . Page::MAX_NAME_LENGTH . " caractères";
                    } else {
                        $page->setName(trim($_POST['name'])); 
                    } 
                        
                    if (empty($errors)) { // if no errors
                    
                        if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                        $pageManager->insert($page); // Insert page into the database
                        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                        header('Location: index.php?page=manage_articles');
                        exit();
                        } else {
                            $errors[] = "Une erreur est survenue au moment de la soumission du formulaire";  // Error invalid token
                        }
                    }
                }
            } else {
                header('Location: index.php?page=login');
            }
             $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update a new token
            require './views/backend/manage_articles.phtml'; // Require the view to add pages
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }

}
