<?php

declare(strict_types=1);

class EventController {

    public function request() {
        
        try {
            $errors = [];
            
            // Get the current date
            $currentDate = new DateTime();
            
            // Select event in 30 days
            $newDate = $currentDate->modify('-30 days');
            
             // Extract date components
            $year = $newDate->format('Y');
            $month = $newDate->format('m');
            $day = $newDate->format('d');
            
            // Format date according to our needs
            $formattedDate = "$year-$month-$day";
            
            // URL de base
            $baseURL = "http://ist-sc3-geobs.osug.fr:8080/fdsnws/event/1/query";
            
            // URL parameters
            $parameters = array(
                // 'starttime' => urlencode("$formattedDate" . "T00:00:00"),
                'starttime' => urlencode("$formattedDate"),
                // 'eventtype' => 'earthquake',
                'maxmagnitude' => '6.5',
                'contributor' => 'ISTerre',
                'format' => 'xml',
                'nodata' => '404'
            );
            
            // Url with parameters
            $url = $baseURL . '?' . http_build_query($parameters);
            
            // HTTP request to obtain the XML response
            $response = file_get_contents($url);
            
            if ($response !== false) {
              // Load XML data from response
                $data = simplexml_load_string($response);
                
                 // Go to the section relating to events
                $eventParameters = $data->eventParameters;
                
                // Convert event data to array
                $result = [];
                
                foreach ($eventParameters->event as $event) {
                    $item = [];
            
                    // Extract useful data from each event
                    $item['publicID'] = (string) $event['publicID'];
                    $item['description'] = (string) $event->description->text;
                    $item['time'] = (string) $event->origin->time->value;
                    $item['type'] = (string) $event->type;
                    $item['magnitude'] = (string) $event->magnitude->mag->value;
                    $item['depth'] = (string) $event->origin->depth->value;
                    $item['evaluationMode'] = (string) $event->origin->evaluationMode;
                    $item['longitude'] = (string) $event->origin->longitude->value;
                    $item['latitude'] = (string) $event->origin->latitude->value;
                    
                  $result[] = $item;  
                }
    
                echo json_encode($result);
                
            } else {
                $errors[] = "Un problème est survenu dans la recherche merci de réessayer ultérieurement";
            }
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
}
