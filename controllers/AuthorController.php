<?php

declare(strict_types=1);

require_once './models/Author.php';
require_once './models/AuthorManager.php';

class AuthorController {
    
    /**
     * Add a new author through the backoffice.
     * This function insert new author submit through the private form into the database 
     * 
     * @param void
     * 
     * @return void
     */
    public function add(): void {
        
        try {
            
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 1) { // Check user rigth : only for webmaster 
                
                $authManager = new AuthorManager();
                $allAuthors = $authManager->selectAll();
                
                if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                    
                    $author = new Author(); // Initialize a Author object to store values
                    
                    if(!empty($_POST)){  // Check not empty form fields 
                        $errors = [];
                        foreach($_POST as $key => $data) {
                            if(empty(trim($data))) {
                                $errors[$key] = 'Le champ '.$key.' ne doit pas être vide.';
                            }
                        }
                    }
                    
                    if (isset($_POST["name"]) && strlen($_POST['name']) > Author::MAX_LOGIN_LENGTH){ // Check and set the name
                        $errors[] = "le nom d'auteur dépasse la longueur autorisée.";
                    } else {  
                        $author->setName(trim($_POST['name'])); 
                    }
    
                    if (isset($_POST["email"]) && !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) { // Check and set the email
                        $errors[] = "email non valide.";
                    } else {
                        $author->setEmail($_POST['email']);
                    }
                    
                    if (isset($_POST["password"])) { // Check and set the passeword
                        
                        $validatePassword = $author->validatePassword(trim($_POST['password']));
                        
                        if ($validatePassword === true) {
                            $author->setPassword(trim($_POST['password']));
                        } else {
                            foreach ($validatePassword as $error)
                            $errors[] = $error . "<br>";
                        }     
                    }         
                    
                    if (isset($_POST["login"]) && strlen($_POST['login']) > Author::MAX_LOGIN_LENGTH){ // Check and set the login name
                        $errors[] = "le login dépasse la longueur autorisée.";
                    } else {  
                        $author->setLogin(trim($_POST['login'])); 
                    }
              
                    if(count($errors) === 0) {
       
                        $newAuthor = $authManager->isExisting($author); // check if auhor is already existing
                        
                        if(isset($newAuthor['login']) && $newAuthor['login'] === $author->getLogin()) {
                            $errors[] ='login déjà existant';
                        } 
                        
                        if (isset($newAuthor['email']) && $newAuthor['email'] === $author->getEmail()) {
                            $errors[] ='email déjà existant'; 
                        } 
                    
                        if (empty($errors)) { // if no errors -> Insert in database -> insertAuth(parameters)
                            $author->setRole((int)$_POST['role']);
                            if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                                $authManager->insert($author);
                                $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                                header('Location: index.php?page=manage_authors'); // Redirect after successful processing
                            }
                        }
                    }
                }
            } else {
                header('Location: index.php?page=login');
            }
        
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
        
        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update 
        require './views/backend/manage_authors.phtml';
    } 
    
    /**
     * Login and Logout to backoffice
     * This function allows you to connect and disconnect from the backoffice.
     * 
     * @param void
     * 
     * @return void
     */
    public function connect(): void {
        
        try {
        
            if($_GET['page'] === 'logout') {
                session_destroy();
                header('Location: index.php');
            }
    
            if(!empty($_POST)){
                
                $errors = [];
                foreach($_POST as $key => $data) {
                    if(trim($data) === '') {
                        $errors[$key] = 'Le champ '.$key.' est requis.';
                    }
                }
                
                $author = new Author(); // Initialize an Author object to store values
                $author->setLogin($_POST['identifier']);
                $author->setEmail($_POST['identifier']);
                
                $enteredPassword = $_POST['password'];
                
                $authManager = new AuthorManager();
                $newAuthor = $authManager->isExisting($author); // check if auhor is already existing
                
                if(!$newAuthor) {
                    $errors[] ="Identifiant ou mot de passe incorrect.";
                } else { // if existing, verify passeword and connect author
                    $storedHashedPassword = $newAuthor['password'];
                    if(password_verify($enteredPassword, $storedHashedPassword)) {
                        if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                            $_SESSION['id'] = $newAuthor['id'];
                            $_SESSION['login'] = $newAuthor['login'];
                            $_SESSION['role'] = $newAuthor['role'];
                            $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                            header("location:index.php?page=manage_requests");
                        } else {
                            $errors[] = "Une erreur est survenue au moment de la soumission du formulaire";  // Error invalid token
                        }
                    } else {
                       $errors[] ="Identifiant ou mot de passe incorrect.";
                    }
                }
            }
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
        
        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update 
        require './views/backend/login.phtml';
    }
    
    /**
     * Update author in the backend.
     * This function allows an author to update his information and update the database.
     * 
     * @param void
     * 
     * @return void
     */
    public function update(): void {
        
        try {
        
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 2) { // Check user rigth 
            
            $authManager = new AuthorManager();
            $author = new Author();
            $author = $authManager->selectOneById($_SESSION['id']); // Retrieve the data of the connected author from the session
            
            // Store the old values for the view
            $oldName  = $author['name'];
            $oldLogin = $author['login'];
            $oldEmail = $author['email'];
            
                if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                    
                    $updateAuthor = new Author(); // Initialize a update Author object to store new values
                    
                    if(!empty($_POST)){ // Check not empty form fields 
                        
                        $errors = [];
                        
                        foreach($_POST as $key => $data) {
                            if(trim($data) === '') {
                                $errors[$key] = 'Le champ '.$key.' est requis.';
                            }
                        }
                        
                        if (isset($_POST["name"]) && strlen($_POST['name']) > Author::MAX_LOGIN_LENGTH) {   // Check and set the new name
                            $errors[] = "le nom d'auteur dépasse la longueur autorisée.";
                        } else {  
                            $updateAuthor->setName(trim($_POST['name']));  
                        }
                        
                        if (isset($_POST["login"])  && strlen($_POST['login']) > Author::MAX_LOGIN_LENGTH) {    // Check and set the new login
                            $errors[] = "le login dépasse la longueur autorisée.";
                        } else {    
                            $updateAuthor->setLogin(trim($_POST['login'])); 
                        }
                         
                        if (isset($_POST["email"]) && !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {  // Check and set the new email
                            $errors[] = "email non valide.";
                        } else { 
                            $updateAuthor->setEmail($_POST['email']);
                        }
                        
                        if ($updateAuthor->getLogin() !== $oldLogin || $updateAuthor->getEmail() !== $oldEmail ) {  // If the values have changed, check if they are unique
                            
                            $existingAuthor = $authManager->isExisting($updateAuthor); 
                            
                            if(isset($existingAuthor['login']) && $existingAuthor['login'] === $updateAuthor->getLogin()) {
                                $errors[] ='login déjà existant';
                            } 
                                
                            if (isset($existingAuthor['email']) && $existingAuthor['email'] === $updateAuthor->getEmail()) {
                                $errors[] ='email déjà existant'; 
                            }
                        }
                        
                        if (isset($_POST["password"])) {
                                    
                                $validatePassword = $updateAuthor->validatePassword(trim($_POST['password']));
                                    
                                    if ($validatePassword === true) {
                                        $updateAuthor->setPassword(trim($_POST['password']));
                                    } else {
                                        foreach ($validatePassword as $error)
                                        $errors[] = $error . "<br>";
                                    }     
                        }   
                        
                        if(count($errors) === 0) {   // If there are no errors, update the author's data

                            if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                                $updateAuthor->setId($_SESSION['id']);
                                $authManager->update($updateAuthor);
                                $_SESSION['login'] = $updateAuthor->getName();
                                $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                                header('Location: index.php?page=author');
                            }
                        }
                        
                    }
                        $author = $authManager->selectOneById($_SESSION['id']);
                }
            } else {
                header('Location: index.php?page=login');  // Redirect if user role is not valid
            }
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
        
        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update 
        require './views/backend/author.phtml';
    }
    
    /**
     * Show authors in the backend for management.
     * This function retrieve and displays all authors
     *  
     * @param void
     * 
     * @return void
     */
    public function manage(): void {
        
        try {
            
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 1) {  // Check user right
                $authManager = new AuthorManager(); // Instantiate a AuthorManager and retrieve all author from the database
                $allAuthors = $authManager->selectAll();
            } else {
                header('Location: index.php?page=login');  // Redirect if user role is not valid
            }
            require './views/backend/manage_authors.phtml'; // Require the view to display messages and management options
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
    
    /**
     * Delete an author 
     * 
     * @param void
     *
     * @return void
     */
    public function delete(): void { 
        
        $errors = []; // Errors array for display
        
        try {
            
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 1) { // Check user rigth 
                
                if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                        
                        if (isset($_POST["delete_author"]) && !empty($_POST["delete_author"])) { // Target author based on POST data
                            $authorId = (int)$_POST['delete_author']; 
                        }  else {
                            $errors[] = "Une erreur s'est produite";  
                        }
                            
                        if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                            $author = new Author(); // Create a new author object and set its ID
                            $author ->setId($authorId);
                            $authManager = new AuthorManager(); // Instantiate a AuthorManager and call the delete method
                            $authManager->delete($author); // Delete author in database
                                
                            $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                        } else {
                            $errors[] = "Une erreur s'est produite"; // Error invalid token
                        }
                        
                        header('Location: index.php?page=manage_authors'); // Redirect after successful processing
                        exit();
                } else {
                        $errors[] = "Vous n'avez pas les droits pour supprimer les articles, veuillez contact votre webmaster"; // Error invalid user rigth
                }
            } else {
                header('Location: index.php?page=login'); // Redirect if user role is not valid
            }
            $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update a new token
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
        
}
