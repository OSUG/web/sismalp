<?php

declare(strict_types=1);

require_once './models/Page.php';
require_once './models/PageManager.php';
require_once './models/ArticleManager.php';
require_once './models/Article.php';
require_once './models/Upload.php';

class ArticleController {
    
    /**
     * Add a new article through the backoffice.
     * This function insert article submit through the private form into the database 
     * 
     * @param void
     * 
     * @return void
     */
    public function add(): void {
        
        try {
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 2) {  // Check user right
                $articleManager = new ArticleManager(); //Instantiate a ArticleManager
                $allArticles = $articleManager->selectAll();
                $pageManager = new PageManager(); //Instantiate a PageManager
                $allPages = $pageManager->selectAll();
                
                $errors = []; // Table for storing errors
                
                if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                    $article = new Article(); 
                    $minLength = 8;
                    
                     // Validate and set content if existing
                    if (isset($_POST['content']) && empty($_POST['content']) || strlen($_POST['content']) < $minLength) {
                        $errors[] = "Un contenu textuel minimum est requis";
                    } else {
                        $article->setContent(trim($_POST['content']));
                    }
                    
                     // Validate and set title if existing
                    if (isset($_POST['title']) && empty($_POST['title'])) {
                        $article->setTitle(null); // field not required, null value if empty
                    } else if (!empty($_POST['title']) && strlen($_POST['title']) < $minLength) {
                        $errors[] = "Le titre doit avoir au moins $minLength caractères";
                    } else {
                        $article->setTitle(trim($_POST['title']));
                    } 
                    
                     // Validate and set url if existing
                    if (isset($_POST['link_url']) && empty($_POST['link_url'])) {
                        $article->setLinkUrl(null); // field not required, null value if empty
                    } else if (!empty($_POST['link_url']) && !filter_var($_POST['link_url'], FILTER_VALIDATE_URL)){
                        $errors[] = "L'URL du lien n'est pas valide";
                    } else {
                        $article->setLinkUrl(trim($_POST['link_url']));
                    }
                    
                     // Validate and set url name if existing
                    if (isset($_POST['link_name']) && empty($_POST['link_name']) && empty($_POST['link_url'])) {
                        $article->setLinkName(null); // field not required, null value if empty
                    } else if (empty($_POST['link_name']) && !empty($_POST['link_url'])) {
                        $errors[] = "Le champ nom du lien est vide";
                    } else if (!empty($_POST['link_name']) && strlen($_POST['link_name']) < $minLength && !empty($_POST['link_url']) ) {
                        $errors[] = "Le nom du lien doit avoir au moins $minLength caractères";
                    } else {
                        $article->setLinkName(trim($_POST['link_name']));
                    }   
                    
                     // Validate and set image if existing
                    if (isset($_FILES['img']['name']) && empty($_FILES['img']['name'])) {
                        $article->setImg(null); // field not required, null value if empty
                    } else if ($_FILES['img']['error'] === 0) {
                            $fileName = $_FILES['img']['name'];
                            $fileSize = $_FILES['img']['size'];
                            $fileTemporyLocation = $_FILES['img']['tmp_name'];
                            
                            $upload = new Upload(); 
                            $newFileName = $upload->validateRenameAndUploadFile($fileName, $fileSize, $fileTemporyLocation);
                            
                            if (is_array($newFileName)) {
                                foreach ($newFileName as $error) {  // Add upload errors to errors array
                                    $errors[] = $error;
                                }
                            } else if ($newFileName) {
                                $article->setImg($newFileName);
                            }
                    } else {
                        $errors[] = "Erreur lors de l'upload du fichier";
                    }
                
                    if (empty($errors)) { // if no errors
                        $article->setAuthorId($_SESSION['id']);
                        $article->setPageId((int)$_POST['page_id']);
                        
                        if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                            $articleManager->insert($article); // Insert article into the database
                            $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                            header('Location: index.php?page=manage_articles');
                            exit();
                        } else {
                            $errors[] = "Une erreur est survenue au moment de la soumission du formulaire";  // Error invalid token
                        }
                    }
                }
    
            } else {
                header('Location: index.php?page=login');
            }
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        } 
        
        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update 
        require './views/backend/manage_articles.phtml';
    }
    
    /**
     * Show articles by page in frontend.
     * This function retrieve and displays all articles by page
     * 
     * @param void
     * 
     * @return void
     */
    public function show(): void {
        try {
            if (isset($_GET['page'])) {
                
                $page = ucfirst(str_replace('_', ' ', $_GET['page'])); 
                
                $articleManager = new ArticleManager();
                $articles = $articleManager->selectByPages($page);
               
                           
                if (isset($articles)) {
                    if ($_GET['page'] === 'a_propos') {
                        require './views/frontend/a_propos.phtml';
                    } elseif ($_GET['page'] === 'partenaires') {
                        require './views/frontend/partenaires.phtml';
                    } elseif ($_GET['page'] === 'template_new_page') { // rename the page to match the one added to the database
                        require './views/frontend/template_new_page.phtml';  // Rename file name page to match the one added to the database (modify the name in routes file)
                    }
                }
            }
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    } 
    
    /**
     * Show articles in the backend for management.
     * This function retrieve and displays all articles
     *  
     * @param void
     * 
     * @return void
     */
    public function manage(): void {
        
        try {
            
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 2) {  // Check user right
                $articleManager = new ArticleManager(); // Instantiate a ArticleManager and retrieve all articles from the database
                $allArticles = $articleManager->selectAll();
                $pageManager = new PageManager(); //Instantiate a PageManager
                $allPages = $pageManager->selectAll();
            } else {
                header('Location: index.php?page=login');  // Redirect if user role is not valid
            }
            require './views/backend/manage_articles.phtml'; // Require the view to display messages and management options
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
    
    public function selectArticleById() {
        
        try {
        
            $content = file_get_contents("php://input");
            $data = json_decode($content, true);
            
            $articleId = $data['articleIdResult'];
            
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 2) { // Check user rigth 
    
                $articleManager = new ArticleManager(); // Instantiate a ArticleManager and retrieve all articles from the database
                $articleData = $articleManager->selectOneById($articleId);
                $pageManager = new PageManager(); //Instantiate a PageManager
                $allPages = $pageManager->selectAll();
                
                $data = [
                    'articleData'   => $articleData,
                    'allPages'      => $allPages,
                ];
                
                header('Content-Type: application/json');
                echo json_encode($data);
                
                // include './views/backend/manage_articles.phtml';
            
            } else {
                header('Location: index.php?page=login'); // Redirect if user role is not valid
            }
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
        
    }
    
    /**
     * Update an article 
     * 
     * @param void
     *
     * @return void
     */
    public function update(): void {
        
        try {
        
        if (isset($_SESSION['role']) && $_SESSION['role'] <= 2) { // Check user rigth 
            $articleManager = new ArticleManager();
            $articleToUpdate = $articleManager->selectOneById($_POST['article_id']);
            $pageManager = new PageManager(); //Instantiate a PageManager
            $allPages = $pageManager->selectAll();
            
            $oldNameImg = $articleToUpdate['img'];
            
            $errors = []; // Table for storing errors
            
            if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                $article = new Article(); 
                $minLength = 8;
                
                if (isset($_POST['article_id']) && !empty($_POST['article_id'])) { // Target article based on POST data
                     $article->setId((int)$_POST['article_id']); 
                }  else {
                     $errors[] = "Une erreur s'est produite";  
                }
                
                 // Validate and set content if existing
                if (isset($_POST['content']) && empty($_POST['content']) || strlen($_POST['content']) < $minLength) {
                    $errors[] = "Un contenu textuel minimum est requis";
                } else {
                    $article->setContent(trim($_POST['content']));
                }
                
                 // Validate and set title if existing
                if (isset($_POST['title']) && empty($_POST['title'])) {
                    $article->setTitle(null); // field not required, null value if empty
                } else if (!empty($_POST['title']) && strlen($_POST['title']) < $minLength) {
                    $errors[] = "Le titre doit avoir au moins $minLength caractères";
                } else {
                    $article->setTitle(trim($_POST['title']));
                } 
                
                 // Validate and set url if existing
                if (isset($_POST['link_url']) && empty($_POST['link_url'])) {
                    $article->setLinkUrl(null); // field not required, null value if empty
                } else if (!empty($_POST['link_url']) && !filter_var($_POST['link_url'], FILTER_VALIDATE_URL)){
                    $errors[] = "L'URL du lien n'est pas valide";
                } else {
                    $article->setLinkUrl(trim($_POST['link_url']));
                }
                
                 // Validate and set url name if existing
                if (isset($_POST['link_name']) && empty($_POST['link_name']) && empty($_POST['link_url'])) {
                    $article->setLinkName(null); // field not required, null value if empty
                } else if (empty($_POST['link_name']) && !empty($_POST['link_url'])) {
                    $errors[] = "Le champ nom du lien est vide";
                } else if (!empty($_POST['link_name']) && strlen($_POST['link_name']) < $minLength && !empty($_POST['link_url']) ) {
                    $errors[] = "Le nom du lien doit avoir au moins $minLength caractères";
                } else {
                    $article->setLinkName(trim($_POST['link_name']));
                }   
                
                
                if (isset($_FILES['img']['name']) && !empty($_FILES['img']['name'])) {

                    if ($_FILES['img']['error'] === 0) {
                        $fileName = $_FILES['img']['name'];
                        $fileSize = $_FILES['img']['size'];
                        $fileTemporyLocation = $_FILES['img']['tmp_name'];
                        
                        $upload = new Upload(); 
                        $newFileName = $upload->validateRenameAndUploadFile($fileName, $fileSize, $fileTemporyLocation);
                        
                        if (is_array($newFileName)) {
                            foreach ($newFileName as $error) {  // Add upload errors to errors array
                                $errors[] = $error;
                            }
                        } else if ($newFileName) {
                            $article->setImg($newFileName);
                        }
                        
                        if ($oldNameImg && $newFileName) {
                        $fileTargetDirectory = "public/uploads/article_img";
                        $fileExtension = explode('.',  $oldNameImg); 
                        $oldFilePath =  $fileTargetDirectory . '/' . strtolower(end($fileExtension)) . '/' . $oldNameImg;
                            if (file_exists($oldFilePath)) {
                                unlink($oldFilePath); // Delete old file
                            }  
                        }
                        
                    } else {
                        $errors[] = "Erreur lors de l'upload du fichier";
                    }
                } elseif (isset($_POST['keep_image']) && $_POST['keep_image'] === 'on') {
                    $article->setImg($oldNameImg);
                } else {
                    $article->setImg(null);
                }
                
                if (empty($errors)) { // if no errors
                    $article->setAuthorId($_SESSION['id']);
                    $article->setPageId((int)$_POST['page_id']);
                    
                    if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                        $articleManager->update($article); // Insert article into the database
                        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                        header('Location: index.php?page=manage_articles');
                        exit();
                    } else {
                        $errors[] = "Une erreur est survenue au moment de la soumission du formulaire";  // Error invalid token
                    }
                }
            } 
        } else {
            header('Location: index.php?page=login'); // Redirect if user role is not valid
        }
        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update a new token
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
    
    /**
     * Delete an article 
     * 
     * @param void
     *
     * @return void
     */
    public function delete(): void { 
        
        $errors = []; // Errors array for display
        
        try {
            
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 2) { // Check user rigth 
                
                if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Check if the request method is POST
                        
                        if (isset($_POST["article_delete"]) && !empty($_POST["article_delete"])) { // Target article based on POST data
                            $articleId = (int)$_POST['article_delete']; 
                        }  else {
                            $errors[] = "Une erreur s'est produite";  
                        }
                            
                        if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                            $article = new Article(); // Create a new Article object and set its ID
                            $article ->setId($articleId);
                            $articleManager = new ArticleManager(); // Instantiate a ArticleManager and call the delete method
                            $articleManager->delete($article); // Delete Article in database
                                
                            $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                        } else {
                            $errors[] = "Une erreur s'est produite"; // Error invalid token
                        }
                        
                        header('Location: index.php?page=manage_articles'); // Redirect after successful processing
                        exit();
                } else {
                        $errors[] = "Vous n'avez pas les droits pour supprimer les articles, veuillez contact votre webmaster"; // Error invalid user rigth
                }
            } else {
                header('Location: index.php?page=login'); // Redirect if user role is not valid
            }
            $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update a new token
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
    
}
