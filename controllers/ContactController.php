<?php

declare(strict_types=1);

require_once './models/ContactManager.php';
require_once './models/Contact.php';

class ContactController {
    
    /**
     * Add a new contact type to the public contact form.
     * This function inserts a new contact type into the database for display on the public contact form.  
     *
     * @param void
     * 
     * @return void
     */
    public function add(): void {
        
        try {
            if (isset($_SESSION['role']) && $_SESSION['role'] <= 1) { // Check user rigth 
                $contactManager = new ContactManager(); // Instantiate a ContactManager 
                $errors = []; // Table for storing errors
                 
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {  // Check if the request method is POST
                    $contact = new Contact();
                    
                    // Validate and set contact type
                    if (isset($_POST['type']) && empty($_POST['type'])) {
                        $errors [] = "Veuillez saisir un type de contact";
                    } else if (!empty($_POST['type']) && strlen($_POST['type']) > Contact::MAX_NAME_LENGTH) {    
                        $errors[] = "Le type de contact ne peut pas dépasser " . Contact::MAX_NAME_LENGTH . " caractères";
                    } else {    
                        $contact->setType(trim($_POST['type'])); 
                    }
                        
                    if (empty($errors)) { // if no errors
                        
                        if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) { // Check token to prevent CSRF attacks
                        $contactManager->insert($contact); // Insert contact into the database
                        $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update after successful processing
                        header('Location: index.php?page=manage_requests');
                        exit();
                        } else {
                        $errors[] = "Une erreur est survenue au moment de la soumission du formulaire";  // Error invalid token
                        }
                    }    
                }
            } else {
                header('Location: index.php?page=login');
            }
            $_SESSION['token'] = bin2hex(random_bytes(35)); // Generate and update a new token
            require './views/backend/manage_articles.phtml';
            
        } catch (Exception $e) {
        // Handle exceptions
            $errorMessage = "An error occurred: " . $e->getMessage();
            header('Location: index.php?page=404');
            exit();
        }
    }
}
